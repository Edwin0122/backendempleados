CREATE DATABASE DBEmpleado;

USE DBEmpleado;

CREATE TABLE Departamento (
IdDepartamento INT PRIMARY KEY IDENTITY,
Nombre VARCHAR (50),
FechaCreacion DATETIME DEFAULT GETDATE()
);

CREATE TABLE Empleado (
IdEmpleado INT PRIMARY KEY IDENTITY,
NombreCompleto VARCHAR (50),
IdDepartamento INT REFERENCES Departamento(IdDepartamento),
Sueldo INT,
FechaContrato DATETIME,
FechaCreacion DATETIME DEFAULT GETDATE()
);

INSERT INTO Departamento (Nombre) VALUES
('Administracion'),
('Marketing'),
('Ventas'),
('Comercio')

INSERT INTO Empleado (NombreCompleto, IdDepartamento, Sueldo, FechaContrato) VALUES
('Edwin Navas',1,1500000,GETDATE())