using BackEndApi.Models;
using BackEndApi.Services.Implementacion;
using BackEndApi.Services.Contrato;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using BackEndApi.DTOs;
using BackEndApi.Utilidades;
using Microsoft.AspNetCore.Http.HttpResults;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<DbempleadoContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("cadenaSql"));
});


builder.Services.AddScoped<IEmpleadoService, EmpleadoService>();
builder.Services.AddScoped<IDepartamentoService, DepartamentoService>();


builder.Services.AddAutoMapper(typeof(AutpMapperProfile));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}


/*************************LISTA DE DEPARTAMENTOS***************************************************************/
app.MapGet("/departamento/lista", async
    (
        IDepartamentoService _departamentoServicio,
        IMapper _mapper
    ) =>
{
    var listaDepartamento = await _departamentoServicio.GetList();
    var listaDepartamentoDto = _mapper.Map<List<DepartamentoDTO>>(listaDepartamento);

    if (listaDepartamentoDto.Count > 0)
        return Results.Ok(listaDepartamentoDto);
    else
        return Results.NotFound();
    
});

/*************************LISTA DE EMPLEADOS***************************************************************/

app.MapGet("/empleados/lista", async 
    (
        IEmpleadoService _empleadoServicio,
        IMapper _mapper
    ) =>
{
    var listaEmpleados = await _empleadoServicio.GetList();
    var listaEmpleadosDTO = _mapper.Map<List<EmpleadoDTO>>(listaEmpleados);

    if (listaEmpleadosDTO.Count > 0)
        return Results.Ok(listaEmpleadosDTO);
    else
        return Results.NotFound();
});


/*************************GUARDAR DEPARTAMENTOS***************************************************************/

app.MapPost("/empleado/guardar", async
    (
        EmpleadoDTO modelo,
        IEmpleadoService _empleadoServicio,
        IMapper _mapper
    ) =>
{
    var _empleado = _mapper.Map<Empleado>(modelo);
    var _empleadoCreado = await _empleadoServicio.Add(_empleado);

    if (_empleadoCreado.IdEmpleado != 0)
        return Results.Ok(_mapper.Map<EmpleadoDTO>(_empleadoCreado));
    else
        return Results.StatusCode(StatusCodes.Status500InternalServerError);

});


/*************************EDITAR DEPARTAMENTOS***************************************************************/

app.MapPut("/empleado/actualizar/{idEmpleado}",async 
    (
        int idEmpleado,
        EmpleadoDTO modelo,
        IEmpleadoService _empleadoServicio,
        IMapper _mapper
    ) =>
{
    var _empleadoEncontrado = await _empleadoServicio.Get(idEmpleado);
    if(_empleadoEncontrado is null)
        return Results.NotFound();

    var _empleado = _mapper.Map<Empleado>(modelo);

    _empleadoEncontrado.NombreCompleto = _empleado.NombreCompleto;
    _empleadoEncontrado.IdDepartamento = _empleado.IdDepartamento;
    _empleadoEncontrado.Sueldo = _empleado.Sueldo;
    _empleadoEncontrado.FechaContrato = _empleado.FechaContrato;

    var respuesta = await _empleadoServicio.Update(_empleadoEncontrado);

    if (respuesta)
        return Results.Ok(_mapper.Map<EmpleadoDTO>(respuesta));
    else
        return Results.StatusCode(StatusCodes.Status500InternalServerError);


});


/*************************ELIMINAR DEPARTAMENTOS***************************************************************/

app.MapDelete("/empleado/eliminar/{idEmpleado}", async 
    (
        int idEmpleado,
        IEmpleadoService _empleadoServicio
    ) =>
{
    var _empleadoEncontrado = await _empleadoServicio.Get(idEmpleado);

    if (_empleadoEncontrado is null)
        return Results.NotFound();
    
    var respuesta = await _empleadoServicio.Delete(_empleadoEncontrado);

    if (respuesta)
        return Results.Ok(respuesta);
    else
        return Results.StatusCode(StatusCodes.Status500InternalServerError);

});


app.Run();


